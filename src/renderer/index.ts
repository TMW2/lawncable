import { ipcRenderer } from "electron";

const quitBtn = document.getElementById("quit");

quitBtn.addEventListener("click", () => {
  ipcRenderer.send("quit");
});

const minimizeBtn = document.getElementById("minimize");

minimizeBtn.addEventListener("click", () => {
  minimizeBtn.blur();
  ipcRenderer.send("minimize");
});

document.ondragover = document.ondrop = (event) => {
  event.preventDefault();
};

const debugBtn = document.getElementById("debug");

//debugBtn.hidden = true;

debugBtn.addEventListener("click", () => {
  debugBtn.blur();
  ipcRenderer.send("debug");
});

const activityLabel = document.getElementById("activity");
const progressBar = document.getElementById("progress") as HTMLProgressElement;
ipcRenderer.on("status-update", (event: any, status: any) => {
  activityLabel.innerText = status.activity;
  if (status.ActivityIsError) activityLabel.classList.add("error");
  else activityLabel.classList.remove("error");
  progressBar.value = status.progress;
});
