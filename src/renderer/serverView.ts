import { ipcRenderer, shell } from "electron";
import GameServer from "./gameserver/server";
import { switchPage } from "./customEvents";

import CheckNAcceptTOS from "./gameserver/TOSCheck";
import { acceptLastTOS } from "./gameserver/TOSCheck";

let GameServerList: GameServer[] = null;
let SelectedGameserver: GameServer = null;
const playBtn: HTMLButtonElement = document.getElementById(
  "play"
) as HTMLButtonElement;

const TOSDialog: HTMLDialogElement = document.getElementById(
  "TOSDialog"
) as HTMLDialogElement;

playBtn.addEventListener("click", async () => {
  if (await CheckNAcceptTOS(SelectedGameserver)) {
    SelectedGameserver.play();
  } else {
    //Open Please accept TOS window
    TOSDialog.showModal();
  }
});
const TOSCancel: HTMLButtonElement = document.getElementById(
  "TOSCancel"
) as HTMLButtonElement;
TOSCancel.addEventListener("click", function () {
  TOSDialog.close();
});
const TOSOpen: HTMLButtonElement = document.getElementById(
  "TOSOpen"
) as HTMLButtonElement;
TOSOpen.addEventListener("click", function (e) {
  e.preventDefault();
  shell.openExternal(SelectedGameserver.TOSLink);
});
const TOSAccept: HTMLButtonElement = document.getElementById(
  "TOSAccept"
) as HTMLButtonElement;
TOSAccept.addEventListener("click", function () {
  acceptLastTOS(SelectedGameserver);
  SelectedGameserver.play();
  TOSDialog.close();
});

const sidebarReference = document.getElementById("sidebar");
let clickableMenueEntries: HTMLElement[] = [];
function updateView() {
  // This is for updating the ui to new data

  if (GameServerList) {
    // Clear every thing out
    while (sidebarReference.firstChild) {
      sidebarReference.removeChild(sidebarReference.firstChild);
    }

    GameServerList.forEach((server) => {
      sidebarReference.appendChild(server.getMenuEntry());
    });

    //Switch page according localstorage:
    if (GameServerList.length != 0) {
      const selectedServer = localStorage.getItem("selected_server");
      if (selectedServer && selectedServer !== null) {
        if (
          GameServerList.filter((server) => server.name == selectedServer)
            .length == 0
        ) {
          localStorage.removeItem("selected_server");
          switchPage("SERVER", GameServerList[0].name, "INFO");
        } else {
          switchPage("SERVER", selectedServer, "INFO");
        }
      } else {
        //TODO ask if is on special page like global settings first
        switchPage("SERVER", GameServerList[0].name, "INFO");
      }
    }
  }
}

const serverPage = document.getElementById("serverPage");
const switchPageEvent = document.createElement("span");
switchPageEvent.classList.add("switch-page-event");
switchPageEvent.addEventListener("site-changed", (event: CustomEvent) => {
  if (event.detail.sitetype === "SERVER") {
    SelectedGameserver = GameServerList.filter(
      (server) => server.name == event.detail.page
    )[0];
    localStorage.setItem("selected_server", SelectedGameserver.name);

    while (serverPage.firstChild) {
      serverPage.removeChild(serverPage.firstChild);
    }
    let page = SelectedGameserver.pageController.getPage(event.detail.subPage);
    serverPage.appendChild(page);
    // page.classList.add('animated');
    // page.classList.add('fadeIn');
  } else {
    SelectedGameserver == null;
  }

  if (typeof SelectedGameserver === "undefined" || SelectedGameserver == null) {
    // No gameserver selected
    playBtn.hidden = true;
  } else {
    // A gameserver is selected
    playBtn.innerText = `Play ${SelectedGameserver.menuName}`;
    playBtn.hidden = false;
  }
  setBackground();
});
document.getElementById("topbar").appendChild(switchPageEvent);

let peviousSelectedGameserver: any = null;
const contentBackground = document.getElementById("contentBackground");
function setBackground() {
  if (peviousSelectedGameserver !== SelectedGameserver) {
    while (contentBackground.firstChild) {
      contentBackground.removeChild(contentBackground.firstChild);
    }
    if (
      typeof SelectedGameserver === "undefined" ||
      SelectedGameserver == null
    ) {
      contentBackground.hidden = true;
      contentBackground.classList.add("hidden");
    } else {
      if (
        SelectedGameserver.backgrounds[0] &&
        !SelectedGameserver.backgrounds[0].isVideo
      ) {
        const background1 = document.createElement("img");
        background1.src = `./media/server/${SelectedGameserver.backgrounds[0].file}`;
        contentBackground.appendChild(background1);
        background1.classList.add("animated");
        background1.classList.add("fadeIn");
      }
      contentBackground.appendChild(document.createElement("div"));
      contentBackground.hidden = false;
      contentBackground.classList.remove("hidden");
    }
  }
  peviousSelectedGameserver = SelectedGameserver;
}

ipcRenderer.on("status-update", (event: any, status: any) => {
  // Everything that isnt server view related is in index.ts
  playBtn.disabled = status.playing;
});

import { GameServers } from "./gameserver/data";
GameServerList = GameServers;
updateView();
