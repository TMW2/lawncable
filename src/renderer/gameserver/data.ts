// This class returns Data over the gameservers until we find a better solution
import GameServer from "./server";
import GameServerProfile from "./profile";
import { NewsType } from "./news";
import { OnlineListParser } from "./onlineCount";
import { socialLink } from "./socialLink";

const TMW2 = new GameServer(
  new GameServerProfile("server.tmw2.org", 6901, "evol2", "TMW2"),
  "TMW2",
  "TMW2: Moubootaur Legends",
  "What happens if you mix TMW with Evol and add many new ideas? ...Ready to become a Moubootaur Legend now?",
  "https://tmw2.org/news",
  { url: "http://updates.tmw2.org/news.txt", type: NewsType.ManaPlus },
  [{ isVideo: false, file: "tmw2/background1.png" }],
  "tmw2/icon.png",
  [
    new socialLink("fas fa-home", "Website", "https://moubootaurlegends.org"),
    new socialLink("fab fa-discord", "Discord", "https://discord.gg/J4gcaqM"),
    new socialLink(
      "fab fa-patreon",
      "Patreon",
      "https://www.patreon.com/TMW2/"
    ),
    new socialLink(
      "fas fa-comments",
      "IRC Chat",
      "http://webchat.freenode.net/?channels=#tmw2"
    ),
    new socialLink(
      "fab fa-youtube",
      "Youtube",
      "https://www.youtube.com/channel/UCiwW5dTmi2xMV_vJfj5Mq5Q"
    ),
    new socialLink(
      "fab fa-steam",
      "Steam Group",
      "http://steamcommunity.com/groups/TheManaWorld2"
    ),
    new socialLink(
      "fab fa-twitter",
      "Twitter",
      "https://twitter.com/TheManaWorld2"
    ),
    new socialLink("fab fa-gitlab", "Gitlab", "https://gitlab.com/TMW2"),
  ],
  "https://tmw2.org/legal",
  { parser: OnlineListParser.JSON_ARRAY, url: "https://tmw2.org/online.json" }
);

const Evol = new GameServer(
  new GameServerProfile("revolt.themanaworld.org", 6969, "evol2"),
  "TMW rEvolt",
  "The Mana World: rEvolt",
  "Join epic adventures with people from all over the world.",
  "https://themanaworld.org/",
  { url: "http://evol.gitlab.io/clientdata/news.txt", type: NewsType.ManaPlus },
  [{ isVideo: false, file: "evol/background1.png" }],
  "evol/icon.png",
  [
    new socialLink("fas fa-home", "Website", "https://themanaworld.org/"),
    new socialLink("fas fa-users", "Forum", "https://forums.themanaworld.org/"),
    new socialLink("fab fa-discord", "Discord", "https://discord.gg/UnY77dR"),
    new socialLink(
      "fas fa-comments",
      "IRC Chat",
      "http://webchat.freenode.net/?channels=#themanaworld"
    ),
    new socialLink(
      "fab fa-youtube",
      "Youtube",
      "https://www.youtube.com/user/themanaworldofficial"
    ),
    new socialLink(
      "fab fa-steam",
      "Steam Group",
      "http://steamcommunity.com/groups/themanaworld"
    ),
    new socialLink(
      "fab fa-gitlab",
      "Gitlab",
      "https://gitlab.com/themanaworld"
    ),
  ],
  "http://policies.themanaworld.org/rules",
  null
);

const TMW = new GameServer(
  new GameServerProfile("server.themanaworld.org", 6901, "tmwAthena"),
  "The Mana World",
  "The Mana World: Legacy",
  "The clasic TMW experience. Join adventures with people from all over the world.",
  "https://www.themanaworld.org/news",
  {
    url: "http://themanaworld.github.io/tmwa-server-data/news.txt",
    type: NewsType.ManaPlus,
  },
  [{ isVideo: false, file: "tmw/background1.png" }],
  "tmw/icon.png",
  [
    new socialLink("fas fa-home", "Website", "https://www.themanaworld.org/"),
    new socialLink("fas fa-users", "Forum", "https://forums.themanaworld.org/"),
  ],
  "https://wiki.themanaworld.org/index.php/The_Mana_World:Terms_and_Conditions",
  { parser: OnlineListParser.TMW, url: "https://server.themanaworld.org/" }
);
// https://policies.themanaworld.org/tos is still offline

const noServer = new GameServer(
  new GameServerProfile("noServer", 6901, "tmwAthena"),
  "Mana Plus",
  "Open Mana Plus",
  "Opens the ManaPlus client",
  "http://manaplus.org/changelog",
  undefined,
  [{ isVideo: false, file: "noServer/background1.png" }],
  "noServer/icon.svg",
  [
    new socialLink("fas fa-home", "Website", "http://manaplus.org/"),
    new socialLink(
      "fab fa-gitlab",
      "Gitlab",
      "https://gitlab.com/themanaworld/manaplus/manaplus/"
    ),
  ],
  undefined,
  null
);

export const GameServers = [TMW2, Evol, TMW, noServer];
