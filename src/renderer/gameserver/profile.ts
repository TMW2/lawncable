export default class GameServerProfile {
  constructor(
    public address: string,
    public port: number,
    public engine: "tmwAthena" | "hercules" | "evol2",
    public serverID: string = "DEFAULT" // To enable some server specific features
  ) {}
}
