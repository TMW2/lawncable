import GameServer from "../server";

export class GameServerPage {
  constructor(protected server: GameServer) {}
  getPage(): HTMLElement {
    const content = document.createElement("div");
    content.classList.add("unknownServerPage");
    content.innerText = `Unknown page for${this.server.name}`;
    return content;
  }
}
