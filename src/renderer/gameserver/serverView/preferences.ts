import GameServerProfile from "../profile";
import { GameServerPage } from "./serverPage";

export class ServerPreferencesPage extends GameServerPage {
  getPage(): HTMLElement {
    const content = document.createElement("div");
    content.classList.add("preferencesServerPage");

    const title = document.createElement("h2");
    title.innerText = `Preferences for ${this.server.name}`;
    content.appendChild(title);

    const loginSection = document.createElement("div");
    loginSection.classList.add("loginSection");
    content.appendChild(loginSection);

    const loginUsernameLabel = document.createElement("label");
    loginUsernameLabel.innerText = "Username";
    const loginUsername = document.createElement("input") as HTMLInputElement;
    loginUsername.value = localStorage.getItem(
      "2_username_" + this.server.profile.address
    );
    loginUsernameLabel.appendChild(loginUsername);

    const loginPinLabel = document.createElement("label");
    loginPinLabel.innerText = "Password";
    const loginPin = document.createElement("input") as HTMLInputElement;
    loginPin.type = "password";
    loginPin.value = localStorage.getItem(
      "2_pin_" + this.server.profile.address
    );
    loginPinLabel.appendChild(loginPin);

    const saveBtn = document.createElement("button") as HTMLButtonElement;
    saveBtn.innerText = "save";

    saveBtn.addEventListener("click", () => {
      localStorage.setItem(
        "2_username_" + this.server.profile.address,
        loginUsername.value
      );
      localStorage.setItem(
        "2_pin_" + this.server.profile.address,
        loginPin.value
      );
      updateLoginTextElement(this.server.profile.address);
    });

    loginSection.appendChild(loginUsernameLabel);
    loginSection.appendChild(loginPinLabel);
    loginSection.appendChild(saveBtn);

    return content;
  }
}

export function updateLoginTextElement(serverAddress: string): void {
  const account = localStorage.getItem("2_username_" + serverAddress);
  const pin = localStorage.getItem("2_pin_" + serverAddress);
  const element = document.getElementById("LoginText");
  if (account && pin) {
    element.innerHTML = `Logged in as <b>${account}</b>`;
  } else {
    element.innerHTML = "Automatic login not set up";
  }
}
