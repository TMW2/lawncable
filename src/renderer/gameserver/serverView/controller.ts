import GameServer from "../server";
import { updateLoginTextElement, ServerPreferencesPage } from "./preferences";
import { ServerInfoPage } from "./info";
import { GameServerPage } from "./serverPage";
import { ServerScreenshotPage } from "./screenshots";

export type ServerSubPages = "INFO" | "SCREENSHOTS" | "PREF" | "UNKNOWN";

export class PageController {
  unknownPage: GameServerPage;
  infoPage: ServerInfoPage;
  scrnshtPage: ServerScreenshotPage;
  prefPage: ServerPreferencesPage;

  constructor(private server: GameServer) {
    this.unknownPage = new GameServerPage(server);
    this.infoPage = new ServerInfoPage(server);
    this.scrnshtPage = new ServerScreenshotPage(server);
    this.prefPage = new ServerPreferencesPage(server);
  }

  getPage(type: ServerSubPages): HTMLElement {
    let page: GameServerPage = this.unknownPage;
    if (type == "INFO") {
      page = this.infoPage;
    } else if (type == "SCREENSHOTS") {
      page = this.scrnshtPage;
    } else if (type == "PREF") {
      page = this.prefPage;
    }
    const htmlElement = document.createElement("div");
    htmlElement.appendChild(page.getPage());
    updateLoginTextElement(this.server.profile.address);
    return htmlElement;
  }
}
