import { ipcRenderer, shell } from "electron";
import { GameServerPage } from "./serverPage";

export class ServerScreenshotPage extends GameServerPage {
  getPage(): HTMLElement {
    const screenshotContainer = document.createElement("div");
    screenshotContainer.classList.add("screenshotsContainer");
    screenshotContainer.id = "screenshots";

    ipcRenderer.send("getScreenshots", this.server.profile.address);

    return screenshotContainer;
  }
}

ipcRenderer.on(
  "getScreenshots",
  (event: any, data: { dir: string; screenshots: string[] }) => {
    console.log(data);
    const screenshots = document.getElementById("screenshots");
    if (screenshots) {
      // Display screenshots if that tab is open
      if (data.screenshots.length !== 0) {
        data.screenshots.forEach((fileName: string) => {
          const screenshot = document.createElement("div");
          screenshot.classList.add("screenshot");
          screenshots.appendChild(screenshot);
          const img = document.createElement("img");
          img.src = data.dir + fileName;
          screenshot.appendChild(img);

          // const text = document.createElement("span");
          // text.innerText = fileName;
          // screenshot.appendChild(text);
          screenshot.addEventListener("dragstart", (event) => {
            event.preventDefault();
            ipcRenderer.send("dragFileOut", data.dir + fileName);
          });
        });
      } else {
        const nothingHere = document.createElement("p");
        nothingHere.classList.add("nothingHere");
        nothingHere.innerText =
          "There is nothing here, yet. Make some screenshots in Game and come back here. The default key for snaping screenshots is 'P'.";

        screenshots.appendChild(nothingHere);
      }

      const openFolderButton = document.createElement("button");
      openFolderButton.innerText = "Open folder to see all";
      openFolderButton.addEventListener("click", () => {
        shell.openItem(data.dir);
      });
      screenshots.appendChild(openFolderButton);
    }
  }
);
