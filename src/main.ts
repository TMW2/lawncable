import * as path from "path";
import {
  app,
  BrowserWindow,
  ipcMain,
  remote,
  Tray,
  Menu,
  dialog,
} from "electron";

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
export let mainWindow: BrowserWindow;
const isSecondInstance = app.makeSingleInstance(() => {
  // Someone tried to run a second instance, we should focus our window.
  if (mainWindow) {
    if (mainWindow.isMinimized()) mainWindow.restore();
    mainWindow.focus();
  }
});

if (isSecondInstance) {
  app.quit();
}
const createWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 780,
    height: 550,
    minHeight: 475,
    minWidth: 650,
    frame: false,
    icon: path.join(__dirname, "../assets/media/icon.ico"),
  });

  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/../assets/index.html`);

  // Emitted when the window is closed.
  mainWindow.on("closed", () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", createWindow);

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin" && !Status.getStatus().playing) {
    app.quit();
  }
});

app.on("activate", () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

ipcMain.on("quit", (event: any, arg: any) => {
  mainWindow.close();
});

ipcMain.on("minimize", (event: any, arg: any) => {
  mainWindow.minimize();
});

ipcMain.on("debug", (event: any, arg: any) => {
  if (mainWindow.webContents.isDevToolsOpened())
    mainWindow.webContents.closeDevTools();
  else mainWindow.webContents.openDevTools();

  Status.setActivity("Debug menue Toggled");
});

import { Status, EventEmitter } from "./main/status";

import { quit as drpcQuit } from "./main/richpresence";

app.on("quit", () => {
  drpcQuit();
  if (appIcon) appIcon.destroy();
});

let appIcon: Tray = null;

EventEmitter.on("openTray", () => {
  if (!appIcon) {
    const iconName = "../assets/media/plushmouboo.png";
    const iconPath = path.join(__dirname, iconName);
    appIcon = new Tray(iconPath);

    updateTrayIconMenue();
  }
});

function updateTrayIconMenue() {
  if (appIcon && appIcon !== null) {
    let menue: Electron.MenuItemConstructorOptions[] = [];
    if (Status.getStatus().gameRunning) {
      menue.push({
        label: "Open screenshot folder",
        click: () => {
          EventEmitter.emit("Mana:openScreenshotDir");
        },
      });
      menue.push({
        label: "Kill ManaPlus",
        click: () => {
          //TODO Ask the user first to confirm
          const options = {
            type: "warning",
            title: "Kill ManaPlus",
            message: "Are you sure?",
            buttons: ["Yes", "No"],
          };
          dialog.showMessageBox(options, (index) => {
            if (index === 0) {
              EventEmitter.emit("Mana:killMana");
            }
          });
        },
      });
    } else {
      menue.push({
        label: "Close Tray Icon",
        click: () => {
          EventEmitter.emit("closeTray");
        },
      });
    }

    appIcon.setToolTip("Mana Launcher");
    appIcon.setContextMenu(Menu.buildFromTemplate(menue));
  }
}

EventEmitter.on("status", updateTrayIconMenue);

EventEmitter.on("closeTray", () => {
  if (appIcon) {
    appIcon.destroy();
    appIcon = null;
  }
});

import { ManaPlus } from "./main/manaplus/manaplus";

ManaPlus.init();

ipcMain.on("play", async (event: any, arg: any) => {
  if (Status.getStatus().playing) return;
  //console.log("play", arg);
  Status.setGameStatus({ server: arg.address });
  Status.setPlaying(true);
  await ManaPlus.start(arg);
  //Status.showError("Failed To Launch Mana Plus","Not implemented yet!","Launch Manaplus faild: Not Implemented");
  return false;
});

ipcMain.on("dragFileOut", (event: any, filepath: any) => {
  event.sender.startDrag({
    file: filepath,
    icon: path.join(__dirname, "../assets/media/screenshot.png"),
  });
});

EventEmitter.on("reopenWindow", () => {
  if (mainWindow === null) {
    createWindow();
  }
});

EventEmitter.on("closeWindow", () => {
  if (mainWindow !== null) {
    mainWindow.close();
  }
});
