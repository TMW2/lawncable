export interface ManaPlusApp {
  readonly startCommand: string;
  getGameDir(): string;
  getVersion(): Promise<string>;
  isInstalled(): boolean;
  updateAvailable(): Promise<{ isNewVersion: boolean; newestVersion: string }>;
  update(): Promise<any>;
}
