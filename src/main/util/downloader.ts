var fs = require("fs");
var request = require("request");
var progress = require("request-progress");

export type Progress = {
  percent: number;
  speed: number;
  size: {
    total: number;
    transfered: number;
  };
  time: {
    elapsed: number;
    remaining: number;
  };
};

export function download(
  url: string,
  targetLocation: string,
  onprogress: (state: Progress) => void
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    progress(request(url), {
      throttle: 500,
    })
      .on("progress", onprogress)
      .on("error", reject)
      .on("end", resolve)
      .pipe(fs.createWriteStream(targetLocation));
  });
}
