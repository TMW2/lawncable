import * as https from "https";
import * as http from "http";

export function getRequest(url: string): Promise<any> {
  const webrequest: any = url.indexOf("https") !== -1 ? https : http;
  const t1 = Date.now();
  return new Promise((resolve, reject) => {
    webrequest
      .get(url, (res: any) => {
        const { statusCode } = res;

        let error;
        if (statusCode !== 200) {
          error = new Error("Request Failed.\n" + `Status Code: ${statusCode}`);
        }
        if (error) {
          res.resume();
          reject(error);
        } else {
          res.setEncoding("utf8");
          let rawData = "";
          res.on("data", (chunk: any) => {
            rawData += chunk;
          });
          res.on("end", () => {
            try {
              resolve(JSON.parse(rawData));
            } catch (e) {
              reject(e);
            }
          });
        }
      })
      .on("error", (e: Error) => {
        reject(e);
      });
  });
}
